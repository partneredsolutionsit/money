<?php

namespace PartneredSolutionsIT\Money\Currency;

use PartneredSolutionsIT\Money\Currency\AbstractCurrency;

class CADCurrency extends AbstractCurrency
{	
	
	protected $currencyCode = "CAD";
	
	protected $displayName = "Canadian Dollar";
	
	protected $numericCode = 124;
	
	protected $fractionalDigits = 2;
	
	protected $subUnit = 100;	

}