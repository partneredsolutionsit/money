<?php

namespace PartneredSolutionsIT\Money\Currency;

use PartneredSolutionsIT\Money\Currency\AbstractCurrency;

class USDCurrency extends AbstractCurrency
{	
	
	protected $currencyCode = "USD";
	
	protected $displayName = "US Dollar";
	
	protected $numericCode = 840;
	
	protected $fractionalDigits = 2;
	
	protected $subUnit = 100;	

}