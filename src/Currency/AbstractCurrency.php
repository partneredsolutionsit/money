<?php

namespace PartneredSolutionsIT\Money\Currency;

abstract class AbstractCurrency
{
	protected $currencyCode;
	
	protected $displayName;
	
	protected $numericCode;
	
	protected $fractionalDigits;
	
	protected $subUnit;

	public function getCurrencycode()
	{
		return $this->currencyCode;
	}
	
	public function getDisplayName()
	{
		return $this->displayName;
	}		
	
	public function getNumericCode()
	{
		return $this->numericCode;
	}

	public function getFractionalDigits()
	{
		return $this->fractionalDigits;
	}	
	
	public function getSubUnit()
	{
		return $this->subUnit;
	}		
}