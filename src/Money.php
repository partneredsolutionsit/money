<?php

namespace PartneredSolutionsIT\Money;

class Money
{
	protected $amount;
	protected $currency;
	
	public function __construct( $amount, $currency )
	{
		$this->amount = $amount;
		
		$this->currency = $currency;
	}
	
	public function getAmount()
	{
		return $this->amount;
	}
	
	public function getCurrency()
	{
		return $this->currency;
	}
	
    /**
     * Returns a new Money object that represents the two added values.
     *
     * @param \PartneredSolutionsIT\Money\Money $other
     * @return static
     * @throws \PartneredSolutionsIT\Money\CurrencyMismatchException
     * @throws \PartneredSolutionsIT\Money\OverflowException
     */	
	public function add( Money $other )
	{
		$this->assertSameCurrency( $this, $other );
		
		$amount = $this->amount + $other->getAmount();
		
		return $this->newMoney( $amount );		
	}
	
	public function subtract( Money $other )
	{	
		$this->assertSameCurrency( $this, $other );	
		
		$amount = $this->amount - $other->getAmount();
		
		return $this->newMoney( $amount );
	}
	
	//not currently used
	public function divide( $divisor, $mode = PHP_ROUND_HALF_UP )
	{
		return $this->newMoney(
			round( $this->amount / $divisor, 0, $mode )
		);				
	}
	
	public function multiply( $factor, $mode = PHP_ROUND_HALF_UP ) 
	{
		return $this->newMoney(
			round( $factor * $this->amount, 0, $mode )
		);		
	}	
		
	public function equals( Money $other )
	{
		return $this->amount == $other->getAmount();
	}
	
	public function greaterThan( Money $other )
	{
		return $this->amount > $other->getAmount();
	}
	
	public function greaterThanOrEqual( Money $other )
	{
		return $this->amount >= $other->getAmount();
	}
	
	public function lessThan( Money $other )
	{
		return $this->amount < $other->getAmount();
	}
	
	public function lessThanOrEqual( Money $other )
	{
		return $this->amount <= $other->getAmount();
	}
	
	public function inverse()
	{
		return $this->newMoney( -1 * $this->amount );
	}
	
    private function assertSameCurrency( Money $a, Money $b )
    {
        if( $a->getCurrency() != $b->getCurrency() ) 
		{
            throw new \Exception('mismatched currency');
        }
    }	
	
	private function assertIsInteger( $amount )
	{
		if( ! is_int( $amount ) )
		{
			throw new \Exception('not integer');
		}
	}

    /**
     * @param int $amount
     *
     * @return static
     */
    private function newMoney( $amount )
    {
        return new static( $amount, $this->currency );
    }	
}