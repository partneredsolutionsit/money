<?php

namespace PartneredSolutionsIT\Money;

class Currency
{
	protected $currencyCode;
	
	protected $displayName;
	
	protected $numericCode;
	
	protected $fractionalDigits;
	
	protected $subUnit;
	
	protected $currencies = [];
	
	public function __construct( $currencyCode )
	{
		$method = "PartneredSolutionsIT\\Money\\Currency\\" . strtoupper( $currencyCode ) . "Currency";

		if(in_array( $currencyCode, $this->currencies ) )
		{
			$method = $this->currencies[ $currencyCode ];
		}
	
		if( ! class_exists( $method ) )
		{
			throw new \Exception('no method');
		}
	
		$this->loadCurrency( new $method );
		
	}
	
	
	private function loadCurrency( $currency )
	{
		$this->currencyCode = $currency->getCurrencyCode();
		$this->displayName = $currency->getDisplayName();
		$this->numericCode = $currency->getNumericCode();
		$this->fractionalDigits = $currency->getFractionalDigits();
		$this->subUnit = $currency->getSubUnit();
	}
}