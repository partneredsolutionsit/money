<?php
 
use PartneredSolutionsIT\Money\Money;
use PartneredSolutionsIT\Money\Currency;
 
class MoneyTest extends PHPUnit_Framework_TestCase {
 
  public function testMoneyAdditionEquals()
  {
    $money1 = new Money(100, 'usd');
    $money2 = new Money(100, 'usd');
	
	$money3 = $money1->add( $money2 );
    $this->assertSame(200, $money3->getAmount());
  }

  public function testMoneyAdditionNotEquals()
  {
    $money1 = new Money(100, 'usd');
    $money2 = new Money(100, 'usd');
	
	$money3 = $money1->add( $money2 );
    $this->assertNotSame(300, $money3->getAmount());
  }
  
  public function testMoneySubtractEquals()
  {
    $money1 = new Money(300, 'usd');
    $money2 = new Money(100, 'usd');
	
	$money3 = $money1->add( $money2 );
    $this->assertSame(400, $money3->getAmount());
  }

  public function testMoneySubtractNotEquals()
  {
    $money1 = new Money(300, 'usd');
    $money2 = new Money(200, 'usd');
	
	$money3 = $money1->add( $money2 );
    $this->assertNotSame(300, $money3->getAmount());
  }  
  
  public function testMoneySubtractNotEquals()
  {
    $money1 = new Money(300, 'usd');
    $money2 = new Money(200, 'usd');
	
	$money3 = $money1->add( $money2 );
    $this->assertNotSame(300, $money3->getAmount());
  }    
  
}